# Automation Practice Project

## General information
The automation project was developed in Java using the following dependencies:

* Maven
* Selenium
* TestNG 

It was developed for running only in Google Chrome.

## Getting started

To successfully run this project I would like to recommend follow this steps:

1. Download Intellij Community: [Intellij Community Edition](https://www.jetbrains.com/idea/download/#section=mac).
2. Download Selenium Webdriver for your Chrome version: [Selenium Downloads page](https://www.selenium.dev/documentation/webdriver/getting_started/install_drivers/).
3. Check if you have Java installed, if not download it from here: [Java downloads page](https://www.oracle.com/java/technologies/downloads/).
4. Unzip the project.
5. Load the project in IntelliJ.
6. Open pom.xml and reload dependencies
7. In "src/main/resources", you will find the chromedriver for Mac, please, replace it with the one you downloaded (inside the downloaded zip, you will find the chromedriver). 
8. Look for “ContactFormTest” inside “src/test/java/”.
9. Right-click on ContactFormTest.java.
10. Click on “Run ‘ContactFormTest’”.
