
import Config.PropertiesFile;
import Pages.Contact;
import Pages.Home;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ContactFormTest {

    ChromeDriver driver;

    @BeforeMethod
    public void beforeMethod() {
        PropertiesFile.readPropertiesFile();
        driver = new ChromeDriver();
        driver.get("http://automationpractice.com/index.php");
    }

    @AfterMethod
    public void afterMethod(){
        driver.quit();
    }

    @Test(description = "Verify the user can move to Contact Us")
    public void moveToContact() {
        new Home(driver).moveToContact();
        String title = new Contact(driver).getContactTitle();
        Assert.assertEquals(title, "Customer service - Contact us".toUpperCase());
    }

    @Test(description = "Verify the user can send a message")
    public void verifyUserCanSendMessage() {
        new Home(driver).moveToContact();
        String mail = "test@123.com";
        String oRef = "123";
        String subject = "Customer service";
        String message = "test";
        String alert = "Your message has been successfully sent to our team.";
        new Contact(driver)
                .selectSubject(subject)
                .insertEmail(mail)
                .insertOrderReference(oRef)
                .insertMessage(message)
                .sendMessage();
        Assert.assertEquals(new Contact(driver).getSuccessMessageSentAlert(), alert);
    }

    @Test(description = "Verify the user cannot send a message with an invalid email")
    public void verifyUserCannotSendWithInvalidEmail() {
        new Home(driver).moveToContact();
        String mail = "test";
        String oRef = "123";
        String subject = "Customer service";
        String message = "test";
        String error = "Invalid email address.";
        new Contact(driver)
                .selectSubject(subject)
                .insertEmail(mail)
                .insertOrderReference(oRef)
                .insertMessage(message)
                .sendMessage();
        Assert.assertEquals(new Contact(driver).getRequiredFieldError(), error);
    }

    @Test(description = "Verify the user cannot send a message without selecting a subject")
    public void verifyUserCannotSendWithoutSubject() {
        new Home(driver).moveToContact();
        String mail = "test@123.com";
        String oRef = "123";
        String message = "test";
        String error = "Please select a subject from the list provided.";
        new Contact(driver)
                .insertEmail(mail)
                .insertOrderReference(oRef)
                .insertMessage(message)
                .sendMessage();
        Assert.assertEquals(new Contact(driver).getRequiredFieldError(), error);
    }

    @Test(description = "Verify the user can send a message without writing the order reference")
    public void verifyUserCanSendMessageWithoutOR() {
        new Home(driver).moveToContact();
        String mail = "test@123.com";
        String subject = "Customer service";
        String message = "test";
        String alert = "Your message has been successfully sent to our team.";
        new Contact(driver)
                .selectSubject(subject)
                .insertEmail(mail)
                .insertMessage(message)
                .sendMessage();
        Assert.assertEquals(new Contact(driver).getSuccessMessageSentAlert(), alert);
    }

    @Test(description = "Verify the user cannot send a message without writing a message")
    public void verifyUserCannotSendWithoutMessage() {
        new Home(driver).moveToContact();
        String subject = "Customer service";
        String mail = "test@123.com";
        String oRef = "123";
        String error = "The message cannot be blank.";
        new Contact(driver)
                .selectSubject(subject)
                .insertEmail(mail)
                .insertOrderReference(oRef)
                .sendMessage();
        Assert.assertEquals(new Contact(driver).getRequiredFieldError(), error);
    }
}
