package Config;

import java.io.*;
import java.util.Properties;

public class PropertiesFile {

    public static void readPropertiesFile()  {
        Properties prop = new Properties();
        File file = new File("src" +File.separator +"main" + File.separator +"java" + File.separator+"Config" + File.separator+ "config.properties");
        try {
            InputStream input = new FileInputStream(file);
            prop.load(input);
            System.setProperty("webdriver.chrome.driver", prop.getProperty("webdriverLocation"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
