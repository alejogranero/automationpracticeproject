package Targets;

public class TargetContact {

    public static final String contactTitle = "#center_column > h1";
    public static final String subjectSelector = "#id_contact";
    public static final String email = "email";
    public static final String orderReference = "id_order";
    public static final String message = "message";
    public static final String submitMessageButton = "submitMessage";
    public static final String successSentMessage = "#center_column > p";
    public static final String requiredFieldError = "#center_column > div > ol > li";
}
