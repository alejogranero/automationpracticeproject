package Pages;

import Targets.TargetContact;
import Utils.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;


public class Contact extends PageObject {
    public Contact(WebDriver driver) {
        super(driver);
    }

    public String getContactTitle() {
        return driver.findElement(By.cssSelector(TargetContact.contactTitle)).getText();
    }

    public Contact selectSubject(String s){
        Select subject = new Select(driver.findElement(By.cssSelector(TargetContact.subjectSelector)));
        subject.selectByVisibleText(s);
        return this;
    }

    public Contact insertEmail(String mail) {
        driver.findElement(By.id(TargetContact.email)).sendKeys(mail);
        return this;
    }

    public Contact insertOrderReference(String oRef) {
        driver.findElement(By.id(TargetContact.orderReference)).sendKeys(oRef);
        return this;
    }

    public Contact insertMessage(String message) {
        driver.findElement(By.id(TargetContact.message)).sendKeys(message);
        return this;
    }

    public Contact sendMessage() {
        driver.findElement(By.id(TargetContact.submitMessageButton)).click();
        return this;
    }

    public String getSuccessMessageSentAlert(){
        return driver.findElement(By.cssSelector(TargetContact.successSentMessage)).getText();
    }

    public String getRequiredFieldError(){
        return driver.findElement(By.cssSelector(TargetContact.requiredFieldError)).getText();
    }

}
