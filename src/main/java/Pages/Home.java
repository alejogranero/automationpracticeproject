package Pages;

import Targets.TargetHome;
import Utils.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class Home extends PageObject {

    public Home(WebDriver driver) {
        super(driver);
    }

    public void moveToContact(){
        driver.findElement(By.cssSelector(TargetHome.ContactUsButton)).click();
    }
}
